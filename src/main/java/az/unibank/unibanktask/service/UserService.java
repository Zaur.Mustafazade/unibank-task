package az.unibank.unibanktask.service;

import az.unibank.unibanktask.entity.User;
import az.unibank.unibanktask.enums.AppUserRole;
import az.unibank.unibanktask.exception.CustomException;
import az.unibank.unibanktask.repository.UserRepo;
import az.unibank.unibanktask.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepo userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    public String signIn(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getAppUserRoles());
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public String signUp(User user) {
        if (!userRepository.existsById(user.getId())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setAppUserRoles(List.of(AppUserRole.ROLE_ADMIN, AppUserRole.ROLE_CLIENT));
            userRepository.save(user);
            return jwtTokenProvider.createToken(user.getUsername(), user.getAppUserRoles());
        } else {
            throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    public User create(User user){
        User appUser = userRepository.findByUsername(user.getUsername());
        if (appUser == null) {
            throw new CustomException("Username is already in use", HttpStatus.NOT_FOUND);
        }
        return userRepository.save(user);
    }

    public void delete(Long id) {
        Optional<User> appUser = userRepository.findById(id);
        if (appUser == null) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
         userRepository.deleteById(id);
    }

    public Optional<User> findByUser(Long id) {
        Optional<User> appUser = userRepository.findById(id);
        if (appUser == null) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
        return appUser;
    }
}
