package az.unibank.unibanktask.service;

import az.unibank.unibanktask.dto.requiest.AccountReqDTO;
import az.unibank.unibanktask.dto.response.AccountResDTO;
import az.unibank.unibanktask.entity.Account;
import az.unibank.unibanktask.exception.CustomException;
import az.unibank.unibanktask.repository.AccountRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepo accountRepo;
    private final ModelMapper modelMapper;

    public List<AccountResDTO> findAll() {

        List<Account> accountList = accountRepo.findAll();
        if (isNull(accountList) || accountList.isEmpty()) {
            throw new CustomException("Don't have this account", HttpStatus.NOT_FOUND);
        }

        return accountList.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    public Account findByAccount(Long accountNumber){
        Account account = accountRepo.findByAccountNumber(String.valueOf(accountNumber));
        if (account == null) {
            throw new CustomException("Don't have this account", HttpStatus.NOT_FOUND);
        }
        return account;
    }

    public AccountResDTO create(AccountReqDTO dto){
        Account account = accountRepo.findByAccountNumber(dto.getAccountNumber());
        if (account != null) {
            throw new CustomException("Already have this account", HttpStatus.NOT_FOUND);
        }
       Account save = accountRepo.save(modelMapper.map(dto,Account.class));
       return modelMapper.map(save, AccountResDTO.class);
    }

    private AccountResDTO convertToDto(Account account) {
        return AccountResDTO.builder().id(account.getId())
                .accountNumber(account.getAccountNumber())
                .balance(account.getBalance()).build();
    }

}
