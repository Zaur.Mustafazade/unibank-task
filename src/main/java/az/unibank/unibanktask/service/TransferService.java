package az.unibank.unibanktask.service;

import az.unibank.unibanktask.dto.requiest.TransferReqDTO;
import az.unibank.unibanktask.dto.response.Status;
import az.unibank.unibanktask.dto.response.TransferResDTO;
import az.unibank.unibanktask.entity.Account;
import az.unibank.unibanktask.exception.CustomException;
import az.unibank.unibanktask.repository.AccountRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class TransferService {

    private final AccountRepo accountRepo;

    public TransferResDTO createTransfer(TransferReqDTO transferReqDTO) {

        if (transferReqDTO.getCreditAcc().equals(transferReqDTO.getDebitAcc())) {
            throw new CustomException("Error happened when try to make transfer to same account", HttpStatus.BAD_REQUEST);
        }
        Account debitAccount = accountRepo.findByAccountNumber(transferReqDTO.getDebitAcc());

        if (isNull(debitAccount)) {
            throw new CustomException("Your account not found", HttpStatus.BAD_REQUEST);
        }
        if (Double.parseDouble(transferReqDTO.getAmount()) > debitAccount.getBalance().doubleValue() ) {

            throw new CustomException("There is no enough money in account balance", HttpStatus.BAD_REQUEST);
        }
        if (debitAccount.isAccountBlock()) {
            throw new CustomException("Your account is deactive", HttpStatus.BAD_REQUEST);
        }
        Account creditAccount = accountRepo.findByAccountNumber(transferReqDTO.getCreditAcc());
        if (isNull(creditAccount)) {
            throw new CustomException("Transfer account not found", HttpStatus.BAD_REQUEST);
        }

        if (creditAccount.isAccountBlock()) {
            throw new CustomException("Error happened when try to make transfer to deactive account", HttpStatus.BAD_REQUEST);
        }

        return new TransferResDTO(Status.getSuccessMessage());
    }

}
