package az.unibank.unibanktask.dto.requiest;

import az.unibank.unibanktask.entity.Account;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserReqDTO {

    private String username;
    private String email;
    private String password;
    private List<Account> accountList;
}
