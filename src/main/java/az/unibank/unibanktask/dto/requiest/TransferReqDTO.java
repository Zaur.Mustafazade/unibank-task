package az.unibank.unibanktask.dto.requiest;

import az.unibank.unibanktask.enums.Currency;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransferReqDTO implements Serializable {

    private String debitAcc;
    private String creditAcc;
    private String amount;
    private Currency debitCcy;
    private Currency creditCcy;
}
