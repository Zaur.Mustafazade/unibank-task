package az.unibank.unibanktask.dto.requiest;

import az.unibank.unibanktask.entity.User;
import az.unibank.unibanktask.enums.Currency;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountReqDTO {

    private String accountNumber;
    private Currency currents;
    private String balance;
    private User userId;
}
