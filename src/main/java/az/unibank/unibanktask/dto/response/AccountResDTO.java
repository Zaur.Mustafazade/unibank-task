package az.unibank.unibanktask.dto.response;

import az.unibank.unibanktask.entity.User;
import az.unibank.unibanktask.enums.Currency;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class AccountResDTO {

    private Long id;
    private String accountNumber;
    private Currency currents;
    private BigDecimal balance;
    private User userId;
}
