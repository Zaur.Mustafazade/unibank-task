package az.unibank.unibanktask.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Status {

    private String statusMessage;
    private Integer statusCode;

    public Status(String statusMessage, Integer statusCode) {
        this.statusMessage = statusMessage;
        this.statusCode = statusCode;
    }

    public static Status getSuccessMessage() {
        return new Status("Success",1);
    }

}
