package az.unibank.unibanktask.dto.response;

import az.unibank.unibanktask.entity.Account;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UserResDto {
    private Integer id;
    private String username;
    private String email;
    private List<Account> accountList;
}
