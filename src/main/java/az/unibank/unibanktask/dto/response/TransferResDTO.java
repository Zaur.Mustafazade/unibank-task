package az.unibank.unibanktask.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TransferResDTO {
    private Status status;

    public TransferResDTO(Status status) {
        this.status = status;
    }
}
