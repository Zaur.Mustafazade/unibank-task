package az.unibank.unibanktask;

import az.unibank.unibanktask.entity.User;
import az.unibank.unibanktask.enums.AppUserRole;
import az.unibank.unibanktask.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
@RequiredArgsConstructor
public class UnibankTaskApplication implements CommandLineRunner {

    final UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(UnibankTaskApplication.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }



    @Override
    public void run(String... params) throws Exception {
//        User admin = new User();
//        admin.setUsername("admin2");
//        admin.setPassword("admin2");
//        admin.setMail("admin2@email.com");
//        admin.setAppUserRoles(new ArrayList<AppUserRole>(Arrays.asList(AppUserRole.ROLE_ADMIN)));
//
//        userService.signUp(admin);
//
//        User client = new User();
//        client.setUsername("client2");
//        client.setPassword("client2");
//        client.setMail("client2@email.com");
//        client.setAppUserRoles(new ArrayList<AppUserRole>(Arrays.asList(AppUserRole.ROLE_CLIENT)));
//
//        userService.signUp(client);
    }
}
