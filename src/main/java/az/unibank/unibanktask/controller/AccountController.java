package az.unibank.unibanktask.controller;

import az.unibank.unibanktask.dto.requiest.AccountReqDTO;
import az.unibank.unibanktask.dto.requiest.TransferReqDTO;
import az.unibank.unibanktask.dto.requiest.UserReqDTO;
import az.unibank.unibanktask.dto.response.AccountResDTO;
import az.unibank.unibanktask.dto.response.TransferResDTO;
import az.unibank.unibanktask.dto.response.UserResDto;
import az.unibank.unibanktask.service.AccountService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/accounts")
@Api(tags = "accounts")
@RequiredArgsConstructor
@Slf4j
public class AccountController {

    private final AccountService accountService;
    private final ModelMapper modelMapper;

    @GetMapping(value = "/{accountNumber}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${AccountController.findByAccount}", response = AccountResDTO.class, authorizations = { @Authorization(value="apiKey") })
    @ApiResponses(value = {//
            @ApiResponse(code = 400, message = "Something went wrong"), //
            @ApiResponse(code = 403, message = "Access denied"), //
            @ApiResponse(code = 404, message = "The user doesn't exist"), //
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    public AccountResDTO findByAccount(@ApiParam("accountNumber") @PathVariable Long accountNumber) {
        return modelMapper.map(accountService.findByAccount(accountNumber), AccountResDTO.class);
    }
    @PostMapping(name = "/all-account")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${AccountController.findAll}", response = AccountResDTO.class, authorizations = { @Authorization(value="apiKey") })
    @ApiResponses(value = {//
            @ApiResponse(code = 400, message = "Something went wrong"), //
            @ApiResponse(code = 403, message = "Access denied"), //
            @ApiResponse(code = 404, message = "The user doesn't exist"), //
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    public ResponseEntity<List<AccountResDTO>> findAll (@ApiParam("all-account")List<AccountReqDTO> accountReqDTOList){
        return  ResponseEntity.status(HttpStatus.OK).body(accountService.findAll());
    }

    @PostMapping("/createAccount")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${AccountController.createAccount}", response = AccountResDTO.class, authorizations = { @Authorization(value="apiKey") })
    @ApiResponses(value = {//
            @ApiResponse(code = 400, message = "Something went wrong"), //
            @ApiResponse(code = 403, message = "Access denied"), //
            @ApiResponse(code = 404, message = "The user doesn't exist"), //
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    public ResponseEntity<AccountResDTO> createAccount(@Valid @RequestBody AccountReqDTO accountReqDTO){
        AccountResDTO accountResDTO=accountService.create(accountReqDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(accountResDTO);
    }

}
