package az.unibank.unibanktask.controller;

import az.unibank.unibanktask.dto.requiest.TransferReqDTO;
import az.unibank.unibanktask.dto.response.AccountResDTO;
import az.unibank.unibanktask.dto.response.TransferResDTO;
import az.unibank.unibanktask.service.TransferService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/transfer")
@Api(tags = "transfer")
@RequiredArgsConstructor
@Slf4j
public class TransferController {
   private final TransferService transferService;

    @PostMapping(name = "/create")
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${AccountController.create}", response = TransferResDTO.class, authorizations = { @Authorization(value="apiKey") })
    @ApiResponses(value = {//
            @ApiResponse(code = 400, message = "Something went wrong"), //
            @ApiResponse(code = 403, message = "Access denied"), //
            @ApiResponse(code = 404, message = "The user doesn't exist"), //
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})

    public TransferResDTO transferResDTO (@RequestBody TransferReqDTO transferReqDTO){
        return transferService.createTransfer(transferReqDTO);
    }

}
