package az.unibank.unibanktask.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "transfers")
@Entity
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "accountFrom")
    private Double accountFrom;

    @Column(name = "accountTo")
    private Double accountTo;

    @Column(name = "currencyFrom")
    private Double currencyFrom;

    @Column(name = "currencyTo")
    private Double currencyTo;

    @Column(name = "rate")
    private Double rate;

    @Column(name = "amountFrom")
    private Double amountFrom;

    @Column(name = "amountTo")
    private Double amountTo;

    @CreationTimestamp
    private LocalDateTime createdAt;


}
