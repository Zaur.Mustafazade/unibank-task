package az.unibank.unibanktask.repository;

import az.unibank.unibanktask.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,Long> {

    Optional<User> findById(Long id);

    User findByUsername(String username);

    @Transactional
    void deleteById(Long id);
}
