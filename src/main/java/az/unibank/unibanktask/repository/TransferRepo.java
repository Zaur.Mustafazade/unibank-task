package az.unibank.unibanktask.repository;

import az.unibank.unibanktask.entity.Transfer;
import jdk.jfr.Registered;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferRepo extends JpaRepository<Transfer,Long> {
}
