package az.unibank.unibanktask.repository;

import az.unibank.unibanktask.entity.Account;
import az.unibank.unibanktask.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepo extends JpaRepository<Account, Long> {

    Account findByAccountNumber(String accountNumber);

}
